var ui = {
    navControl: function(){
        var trigger= $('.nav__item');
        var trigger2= $('.nav-panel a');
        var trigger3= $('.mobile-nav-trigger');
        var trigger4= $('.mobile-nav__button');

        trigger
        .on('mouseover focusin',function(){
            $(this).find('.nav__button').addClass('active');
            $(this).find('.nav-panel').addClass('active');
            // $(this).find('.nav-panel li a').focus();
        })
        .on('mouseout focusout',function(){
            $(this).find('.nav__button').removeClass('active');
            $(this).find('.nav-panel').removeClass('active');
        });

        trigger2
        .on('mouseover focusin',function(){
            $(this).addClass('active');
        })
        .on('mouseout focusout',function(){
            $(this).removeClass('active');
        });

        trigger3
        .on('click', function(){
            if($('.mobile-nav').is('.active')){
                $('.mobile-nav').removeClass('active').attr({'aria-hidden':'true','role':'none presentation'});
                $('body').removeClass('not-scroll');
            }else{
                $('.mobile-nav').addClass('active').attr({'aria-hidden':'false','role':''})
                $('body').addClass('not-scroll');
            }
        });

        trigger4
        .on('click', function(){
            if($(this).is('.active')){                
                $(this).removeClass('active').next('.mobile-nav-panel').removeClass('active');
            }else{                
                $(this).addClass('active').next('.mobile-nav-panel').addClass('active')
            }
        });
    },
    tabControl: function() {
        $(".tab button").click(function() {
            
            $(".tab button").removeClass('active');
            $(this).addClass('active');
            $(".tab__panel").removeClass('active');

            var tab_id = $(this).attr('id');
            var activeTab = $('[aria-labelledby='+tab_id+']');

            $(activeTab).addClass('active');
        });
    },
    slick :function (obj){
        $(obj).slick({
            speed: 500,
            pauseOnHover: false,
            swipeToSlide: true,
            arrows: true
        });
    },
    slick2 : function (obj){
        $(obj).slick({
            speed: 500,
            pauseOnHover: false,
            swipeToSlide: true,
            arrows: true,
            slidesToShow: 2,
            slidesToScroll: 1,
            responsive: [
                {  
                    breakpoint: 750,
                    settings: {
                        slidesToShow:1,
                        slidesToScroll: 1,
                    } 
                }
            ]

        });
    },
    cardReverse : function (obj){
        var back = $(obj).find('.product-card__back');
        back.attr({'role':'none presentation','aria-hidden':'true'});

        $(obj)
        .on('mouseover focusin',function(){
            var el = $(this);
            var back = el.find('.product-card__back');
            var front = el.find('.product-card__front');
            var button = el.find('.product-card__button-wrap');

            back
            .css({'opacity':'1','transition':'.5s'})
            .attr({'role':'','aria-hidden':'false'});

            front
            .css({'opacity':'0','transition':'.5s'})
            .attr({'role':'none presentation','aria-hidden':'true'});

            button
            .css({'bottom':'50px','transition':'.7s'})
        })
        .on('mouseout focusout',function(){
            var el = $(this);
            var back = el.find('.product-card__back');
            var front = el.find('.product-card__front');
            var button = el.find('.product-card__button-wrap');

            back.css({'opacity':'0','transition':'.5s'})
            .attr({'role':'none presentation','aria-hidden':'true'});

            front.css({'opacity':'1','transition':'.5s'})
            .attr({'role':'','aria-hidden':'false'});

            button
            .css({'bottom':'30px'})
        });
    }
};

/* event */
$(document).bind({
	'ready' : function(){
        ui.tabControl();
        ui.navControl();
        ui.slick('.hero');
        ui.slick2('.info-boxes');
        ui.cardReverse('.product-card');
	},
	'scroll' : function(){
        // ui.headerControl();
	},
	'resize' : function(){
	}
})